import { Injectable } from '@angular/core';
import { assign, cloneDeep } from 'lodash-es';
import { FuseMockApiService } from '@fuse/lib/mock-api';
import { EMPLOYEE_DATA as employeeData } from 'app/mock-api/employee-data/data';
import { DashboardService } from 'app/modules/admin/dashboard/dashboard.service';

@Injectable({
    providedIn: 'root'
})
export class EmployeeMockApi
{
    private employee: any = employeeData;

    /**
     * Constructor
     */
    constructor(private _fuseMockApiService: FuseMockApiService, private _dashboardService: DashboardService)
    {
        // Register Mock API handlers
        this.registerHandlers();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register Mock API handlers
     */
    registerHandlers(): void
    {
        // -----------------------------------------------------------------------------------------------------
        // @ employee - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/common/employee')
            .reply(() => [200, {status: 200, message: "success", data: this.employee}]);
        
        
        // -----------------------------------------------------------------------------------------------------
        // @ employee - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/common/employee/filter')
            .reply(({request}) => {
                const filtered = this.employee.filter(emp => emp.username.includes(request.body))
                this._dashboardService.setEmployeeData = filtered;
                return [200, {status: 200, message: "success", data: filtered}]
            });

        // -----------------------------------------------------------------------------------------------------
        // @ employee - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/common/employee/detail')
            .reply(({request}) => {
                const filtered = this.employee.filter(emp => emp.username.includes(request.body))
                return [200, {status: 200, message: "success", data: filtered}]
            });

        // -----------------------------------------------------------------------------------------------------
        // @ employee - add
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/common/employee')
            .reply(({request}) => {

                // Update the user mock-api
                this.employee.push(request.body)

                this._dashboardService.setEmployeeData = this.employee;

                // Return the response
                return [200, {status: 200, message: "success"}];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ employee - update
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/common/employee/edit')
            .reply(({request}) => {
                let newArr = []
                this._dashboardService.getEmployeeData$.subscribe(res => {
                    const findIndex = res.findIndex(
                        (idx) => (idx.username = request.body.username)
                    );
                    res[findIndex] = {...request.body};
                    newArr = res
                })
                this._dashboardService.setEmployeeData = newArr;
                // Return the response
                return [200, {status: 200, message: "success"}];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ employee - delete
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/common/employee/delete')
            .reply(({request}) => {
                const index = this.employee.findIndex(filtered => filtered.username === request.body.username)
                if (index !== -1) {
                    this.employee.splice(index, 1);
                }

                this._dashboardService.setEmployeeData = this.employee;

                // Return the response
                return [200, {status: 200, message: "success"}];
            });
    }
}
