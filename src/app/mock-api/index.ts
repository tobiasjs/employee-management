
import { ActivitiesMockApi } from 'app/mock-api/pages/activities/api';
import { AuthMockApi } from 'app/mock-api/common/auth/api';
import { IconsMockApi } from 'app/mock-api/ui/icons/api';
import { UserMockApi } from 'app/mock-api/common/user/api';
import { EmployeeMockApi } from './employee-data/api';

export const mockApiServices = [
    ActivitiesMockApi,
    AuthMockApi,
    IconsMockApi,
    UserMockApi,
    EmployeeMockApi
];
