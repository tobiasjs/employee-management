import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import {EMPLOYEE_DATA} from '../../../mock-api/employee-data/data'
import { EmployeeData } from 'app/mock-api/employee-data/types';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class DashboardService {
    private employeeData$: BehaviorSubject<EmployeeData[]> = new BehaviorSubject([]);
    private search$: BehaviorSubject<string> = new BehaviorSubject('');
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient) {}

    get getEmployeeData$(): Observable<EmployeeData[]> {
        return this.employeeData$.asObservable();
    }

    set setEmployeeData(data: EmployeeData[]) {
        this.employeeData$.next(data);
    }

    get getSearch$(): Observable<string>{
        return this.search$.asObservable();
    }

    set setSearch(data: string) {
        this.search$.next(data);
    }

    getEmployeeData(): Observable<any> {
        return this._httpClient.get('api/common/employee').pipe(tap<any>(response => {
            if (response.status === 200) {
                this.employeeData$.next(response.data);
            }
        }));
    }

    addEmployeeData(body): Observable<any> {
        return this._httpClient.post('api/common/employee', body);
    }

    editEmployeeData(body): Observable<any> {
        return this._httpClient.post('api/common/employee/edit', body);
    }

    filterEmployeeData(body:{searchKey:string}): Observable<any> {
        return this._httpClient.post('api/common/employee/filter', body)
    }

    getDetailEmployeeData(body:{searchKey:string}): Observable<any> {
        return this._httpClient.post('api/common/employee/detail', body)
    }

    deleteEmployeeData(body:{username: string}): Observable<any> {
        return this._httpClient.post('api/common/employee/delete', body);
    }
}
